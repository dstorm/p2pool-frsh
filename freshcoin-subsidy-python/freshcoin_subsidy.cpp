#include <Python.h>

static const int64_t COIN = 100000000;

#define HOUR        60   // 60 minutes
#define DAY         24   // 24 hours

int64_t static GetBlockValue(int nHeight)
{
    //1M coins total. 50000 premine, 40 per block after, etc etc
    int64_t reward = 10 * COIN;
    int adjust=30+1; //30 blocks of zero rewards, to adjust difficulty and ensure fair launch (plus 1 for premine)
    int firstreward=adjust+1*HOUR*DAY; //first day of mining 
    int secondreward=firstreward+(2*HOUR*DAY); //next 13 days of mining
    int thirdreward=secondreward+(3*HOUR*DAY); //next 13 days of mining
    int fourthreward=thirdreward+(1*HOUR*DAY); //last day of "primary mining"
    int finalreward=fourthreward+((HOUR*DAY*300)-40); //final reward phase (after this, nothing)
    int64_t sub=0;
    if(nHeight==1)
    {
        sub=5000*reward; //premine of 50000 coins, 0.5% of cap
    }
    else if(nHeight < adjust)
    {
        sub=0;
    }
    else if(nHeight < firstreward)
    {
        sub=reward*100; //bonus phase of 1000 coins 
    }
    else if(nHeight<secondreward)
    {
        sub=reward*50; //normal first phase of 500 coins
    }
    else if(nHeight<thirdreward)
    {
        sub=reward*25; //normal first phase of 250 coins
    }
    else if(nHeight<fourthreward)
    {
        sub=reward*100; //end primary mining with a bang round of 1000 coins
    }
    else if(nHeight<finalreward)
    {
        sub=reward; //final phase is 10 coin reward
    }
    else
    {
        sub=0; //no coins
    }

    return sub;
}

static PyObject *freshcoin_subsidy_getblockvalue(PyObject *self, PyObject *args)
{
    int input_height;
    if (!PyArg_ParseTuple(args, "i", &input_height))
        return NULL;
    long long output = GetBlockValue(input_height);
    return Py_BuildValue("L", output);
}

static PyMethodDef freshcoin_subsidy_methods[] = {
    { "getBlockValue", freshcoin_subsidy_getblockvalue, METH_VARARGS, "Returns the block value" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initfreshcoin_subsidy(void) {
    (void) Py_InitModule("freshcoin_subsidy", freshcoin_subsidy_methods);
}
