from distutils.core import setup, Extension

freshcoin_module = Extension('freshcoin_subsidy', sources = ['freshcoin_subsidy.cpp'])

setup (name = 'freshcoin_subsidy',
       version = '1.0',
       description = 'Subsidy function for FreshCoin',
       ext_modules = [freshcoin_module])
