import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

nets = dict(
    freshcoin=math.Object(
        P2P_PREFIX='c1c1c1c1'.decode('hex'),
        P2P_PORT=22112,
        ADDRESS_VERSION=36,
        RPC_PORT=22111,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'freshcoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: __import__('freshcoin_subsidy').getBlockValue(height+1),
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('fresh_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('fresh_hash').getPoWHash(data)),
        BLOCK_PERIOD=60, # s
        SYMBOL='FRSH',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'FreshCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/FreshCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.freshcoin'), 'freshcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=256,
        DUST_THRESHOLD=0.001e8,
    ),
    freshcoin_testnet=math.Object(
        P2P_PREFIX='fcc1b7fc'.decode('hex'),
        P2P_PORT=11112,
        ADDRESS_VERSION=138,
        RPC_PORT=11111,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'freshcoinaddress' in (yield bitcoind.rpc_help()) and
            (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: __import__('freshcoin_subsidy').getBlockValue(height+1),
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('fresh_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('fresh_hash').getPoWHash(data)),
        BLOCK_PERIOD=60, # s
        SYMBOL='tFRSH',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'FreshCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/FreshCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.freshcoin'), 'freshcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=256,
        DUST_THRESHOLD=0.001e8,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
